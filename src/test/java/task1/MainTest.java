package task1;

import org.junit.Assert;
import org.junit.Test;

public class MainTest extends Assert {

    @Test
    public void ipAddressTest_ISVALID() throws Exception {
        IpAddress ip = new IpAddress();
        assertTrue("некорректный IP адрес", ip.isValid("168.0.0.1"));
    }

    @Test
    public void ipAddressTest_GET_NUMERICAL_VALUE() throws Exception {

        IpAddress ip = new IpAddress();
        assertTrue("у IP адреса 168.0.0.1-значение в long=2818572289", ip.getNumericalValue("168.0.0.1") == 2818572289L);
    }

    @Test
    public void ipAddressTest_INCREMENT() throws Exception {

        IpAddress ipStart = new IpAddress("168.0.0.1");
        IpAddress currentIp = ipStart.increment();

        assertTrue("168.0.0.1 после increment() должен стать 168.0.0.2 (т.е 2818572290L)", currentIp.numericalValue == 2818572290L);
    }
}
