package task2;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PhoneBookTest extends Assert {
    String user = "fio";
    String testPhone = "99999";


    @Test
    public void PhoneBookTest_NULL() throws Exception {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.putUser(user, null);

        assertTrue(phoneBook.getPhones(user) == null);
    }

    @Test
    public void PhoneBookTest_ISEMPTY() throws Exception {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.putUser(user, new ArrayList<>());

        assertTrue("список телефонов должен быть пустым", phoneBook.getPhones(user).isEmpty());
    }

    @Test
    public void PhoneBookTest_HAS_USER() throws Exception {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.putUser("Коновалов К.К.", new ArrayList<>());
        assertTrue("данный пользователь отсутствует в телефонной книге", !phoneBook.hasUser("Козлов К.К."));
        assertTrue("данный пользователь присутствует в телефонной книге", phoneBook.hasUser("Коновалов К.К."));
    }

    @Test
    public void PhoneBookTest_GET_PHONES() throws Exception {
        PhoneBook phoneBook = new PhoneBook();
        List<String> list1 = new ArrayList<>();
        list1.add(testPhone);
        phoneBook.putUser(user, list1);

        assertTrue("у пользователя - fio1 номер телефона 99999 ", phoneBook.getPhones(user).get(0) == testPhone);
        assertTrue(phoneBook.getPhones(user).size() == list1.size());
        assertTrue(phoneBook.getPhones(user).containsAll(list1));
    }
}