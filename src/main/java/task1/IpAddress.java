package task1;

public class IpAddress {
    String ip;
    long numericalValue;

    public IpAddress(String ip) throws Exception {
        if (isValid(ip)) {
            this.ip = ip;
            this.numericalValue = getNumericalValue(this.ip);
        } else {
            throw new Exception("IP is not correct");
        }
    }

    public IpAddress(String ip, long numericalValue) {
        this.ip = ip;
        this.numericalValue = numericalValue;
    }

    public IpAddress() {}

    public static long getNumericalValue(String ip) {
        String[] ipParts = ip.split("\\.");
        long numericalValue = 0;
        for (int i = 0; i < ipParts.length; i++) {
            //Адрес делится на 4 октета, по 8 бит каждый,т.е число 168=10101000, число 0=00000000, число 1=00000001, если соединить и перевести будет число 2818572289
            long i1 = Long.parseLong(ipParts[i]);
            long i2 = i1 << 8 * (3 - i);
            numericalValue += i2;
        }
        return numericalValue;
    }

    public IpAddress increment() {
        String ip = "";
        long numericalValue;

        numericalValue = this.numericalValue + 1;
        for (int i = 0, shift = 8 * (3 - i); i < 4; shift -= 8, i++) {
            long i1 = (numericalValue >> shift) & 0xff;
            ip += (i1 + (i < 3 ? "." : ""));
        }
        return new IpAddress(ip, numericalValue);
    }

    public boolean isValid(String ip) {
        boolean validValue = true;
        if (ip.matches("^[\\d]{1,3}\\.[\\d]{1,3}\\.[\\d]{1,3}\\.[\\d]{1,3}$")) {
            String[] ipPart = ip.split("\\.");
            for (String part : ipPart) {
                short sh = Short.valueOf(part);
                if (sh > 255 || sh < 0) {
                    validValue = false;
                    break;
                }
            }
        } else {
            validValue = false;
        }
        return validValue;
    }
}