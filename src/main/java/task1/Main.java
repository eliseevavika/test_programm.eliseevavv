package task1;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        boolean end = false;
        while (!end) {
            System.out.println("Введите IP-адрес №1:");
            Scanner scanner = new Scanner(System.in);
            String ip1 = scanner.nextLine();
            System.out.println("Введите IP-адрес №2:");
            String ip2 = scanner.nextLine();
            IpAddress ipStart;
            try {
                ipStart = new IpAddress(ip1);
                IpAddress ipEnd = new IpAddress(ip2);
                IpAddress currentIp = ipStart.increment();
                while (currentIp.numericalValue < ipEnd.numericalValue) {
                    System.out.println(currentIp.ip);
                    currentIp = currentIp.increment();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }
}