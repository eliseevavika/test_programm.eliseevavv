package task2;

import java.util.*;


public class Main {
    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();

        List<String> listI = new ArrayList<>();
        listI.add("+8 800 2000 500");
        listI.add("+8 800 200 600");

        List<String> listP = new ArrayList<>();
        listP.add("+8 800 2000 700");

        List<String> listS = new ArrayList<>();
        listS.add("+8 800 2000 800");
        listS.add("+8 800 2000 900");
        listS.add("+8 800 2000 000");


        phoneBook.putUser("Иванов И.И.", listI);
        phoneBook.putUser("Петров П.П.", listP);
        phoneBook.putUser("Сидоров С.С.", listS);


        boolean end = false;
        while (!end) {
            System.out.println("для поиска контакта введите Фамилию И.О.");
            Scanner scanner = new Scanner(System.in);
            String user = scanner.nextLine();
            if (user.equals("end")) {
                end = true;
                continue;
            }
            if (!phoneBook.hasUser(user)) {
                System.out.println("Данного пользователя нет в телефонной книге");
                continue;
            }
            List<String> phones = phoneBook.getPhones(user);
            if (phones.isEmpty()) {
                System.out.println("У данного пользователя отсутствует номер в телефонной книге");
            } else {
                Iterator<String> iter = phones.iterator();

                for (int i = 1; i < phones.size() + 1; i++) {
                    System.out.println(i + ". " + iter.next());
                }
            }

        }
    }
}


