package task2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Виктория on 24.01.2017.
 */
public class PhoneBook {

    private Map<String, List<String>> hashMap = new HashMap<>();

    public void putUser(String user, List phones) {
        hashMap.put(user, phones);
    }


    public List<String> getPhones(String user) {
        return hashMap.get(user);
    }

    public boolean hasUser(String user) {
        return hashMap.containsKey(user);
    }

}
